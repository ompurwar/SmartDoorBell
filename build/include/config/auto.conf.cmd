deps_config := \
	/home/hello/esp/esp-idf/components/app_trace/Kconfig \
	/home/hello/esp/esp-idf/components/aws_iot/Kconfig \
	/home/hello/esp/esp-idf/components/bt/Kconfig \
	/home/hello/esp/esp-idf/components/driver/Kconfig \
	/home/hello/esp/esp-idf/components/esp32/Kconfig \
	/home/hello/esp/esp-idf/components/esp_adc_cal/Kconfig \
	/home/hello/esp/esp-idf/components/esp_http_client/Kconfig \
	/home/hello/esp/esp-idf/components/ethernet/Kconfig \
	/home/hello/esp/esp-idf/components/fatfs/Kconfig \
	/home/hello/esp/esp-idf/components/freertos/Kconfig \
	/home/hello/esp/esp-idf/components/heap/Kconfig \
	/home/hello/esp/esp-idf/components/libsodium/Kconfig \
	/home/hello/esp/esp-idf/components/log/Kconfig \
	/home/hello/esp/esp-idf/components/lwip/Kconfig \
	/home/hello/esp/esp-idf/components/mbedtls/Kconfig \
	/home/hello/esp/esp-idf/components/mdns/Kconfig \
	/home/hello/esp/esp-idf/components/openssl/Kconfig \
	/home/hello/esp/esp-idf/components/pthread/Kconfig \
	/home/hello/esp/esp-idf/components/spi_flash/Kconfig \
	/home/hello/esp/esp-idf/components/spiffs/Kconfig \
	/home/hello/esp/esp-idf/components/tcpip_adapter/Kconfig \
	/home/hello/esp/esp-idf/components/vfs/Kconfig \
	/home/hello/esp/esp-idf/components/wear_levelling/Kconfig \
	/home/hello/esp/esp-idf/Kconfig.compiler \
	/home/hello/esp/esp-idf/components/bootloader/Kconfig.projbuild \
	/home/hello/esp/esp-idf/components/esptool_py/Kconfig.projbuild \
	/home/hello/esp/esp-idf/components/partition_table/Kconfig.projbuild \
	/home/hello/esp/esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)


$(deps_config): ;
