#include "cJSON.h"
#include "MQTTClient.h"
#include "driver/adc.h"
#include "nvs_flash.h"
#include <arpa/inet.h>
#include <esp_adc_cal.h>
#include <esp_err.h>
#include <esp_event.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <esp_spi_flash.h>
#include <esp_system.h>
#include <esp_task_wdt.h>
#include <esp_timer.h>
#include <esp_wifi.h>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>
#include <lwip/sockets.h>
#include <netinet/in.h>
#include <rom/ets_sys.h> // for uS delays
#include <sdkconfig.h>
#include <stdio.h>
#include <tcpip_adapter.h>
#include <stdbool.h>



// telling the compiler about the an eventGroup
EventGroupHandle_t myEventGroup;

// declaring my event bits
extern const int GOT_MY_ADDRESS;
extern const int GOT_PEER_ADDRESS;

// referring the mqtt message vars defined in main.c file
_Bool mqPublish;
char mqttPayload[100];

// udp socket declaration
int sock;
struct sockaddr_in serverAddress;

/**
 * @brief  structur of address
 */
typedef struct address {
  int port;
  char ip[15];
} address;

/**
 * @brief
 * @myAddress : socket address of this device
 * @peerAdderss : socket adddress of peer
 */
address myAddress;
address peerAddress;

/**
 * @brief  function to set peers addresss of the udp socket
 * @note
 * @param  *peerAdderss: pointer to the peerAdderss storeage
 * @param  port: Destination port
 * @param  ip[]: Destination ip address
 * @retval None
 */
void setPeerAddress(struct sockaddr_in *Adderss, int port, char ip[]);

/**
 * @brief   :       mULaw ecoder function
 * @link    :       https://iesc-s2.mit.edu/608/spring18/lab06a#page_anchor_2
 * @param  sample:  data to be encoded
 * @retval :        encoded data
 */
int8_t mulaw_encode_fast(int16_t sample);

/**
 * @brief  mULaw decoder function
 * @link : http://www.speech.cs.cmu.edu/comp.speech/Section2/Q2.7.html
 * @param ulawbyte: byte to be decoded of 8 bits
 * @retval sample: decoded byte of 16 bits
 */
int8_t ulaw2linear(unsigned char ulawbyte);

void sendAddrtoPeer(void);

void GetAdddrsFromJsonRes(char incomJsonRes[]);
