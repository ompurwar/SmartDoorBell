#include "include.h"


// declaring my event bits
const int GOT_MY_ADDRESS = BIT0;
const int GOT_PEER_ADDRESS = BIT1;
/**
 * @brief  function to set peers addresss of the udp socket
 * @note
 * @param  *peerAdderss: pointer to the peerAdderss storeage
 * @param  port: Destination port
 * @param  ip[]: Destination ip address
 * @retval None
 */
void setPeerAddress(struct sockaddr_in *Adderss, int port, char ip[]) {
  serverAddress.sin_family= AF_INET;
  serverAddress.sin_port = htons(port);
  inet_pton(AF_INET, ip, &serverAddress.sin_addr.s_addr);
};







/**
 * @brief   :       mULaw ecoder function
 * @link    :       https://iesc-s2.mit.edu/608/spring18/lab06a#page_anchor_2
 * @param  sample:  data to be encoded
 * @retval :        encoded data
 */
int8_t mulaw_encode_fast(int16_t sample)
{
  const uint16_t MULAW_MAX = 0x1FFF;
  const uint16_t MULAW_BIAS = 33;
  uint16_t mask = 0x1000;
  uint8_t sign = 0;
  uint8_t position = 12;
  uint8_t lsb = 0;
  if (sample < 0)
  {
    sample = -sample;
    sign = 0x80;
  }
  sample += MULAW_BIAS;
  if (sample > MULAW_MAX)
  {
    sample = MULAW_MAX;
  }
  for (; ((sample & mask) != mask && position >= 5); mask >>= 1, position--)
    ;
  lsb = (sample >> (position - 4)) & 0x0f;
  return (~(sign | ((position - 5) << 4) | lsb));
};

/**
 * @brief  mULaw decoder function
 * @link : http://www.speech.cs.cmu.edu/comp.speech/Section2/Q2.7.html
 * @param ulawbyte: byte to be decoded of 8 bits
 * @retval sample: decoded byte of 16 bits
 */
int8_t ulaw2linear(unsigned char ulawbyte) 
{
  static int exp_lut[8] = {0, 132, 396, 924, 1980, 4092, 8316, 16764};
  int sign, exponent, mantissa, sample;
  ulawbyte = ~ulawbyte;
  sign = (ulawbyte & 0x80);
  exponent = (ulawbyte >> 4) & 0x07;
  mantissa = ulawbyte & 0x0F;
  sample = exp_lut[exponent] + (mantissa << (exponent + 3));
  if (sign != 0)
    sample = -sample;

  return (sample);
}



void GetAdddrsFromJsonRes(char incomJsonRes[])
{

  ESP_LOGD("app_main", "[heap size log] %d", esp_get_free_heap_size());
  cJSON *jsonObj = cJSON_Parse(incomJsonRes);
  cJSON *msgType = cJSON_GetObjectItemCaseSensitive(jsonObj, "msgType");
  cJSON *AddressType = cJSON_GetObjectItemCaseSensitive(jsonObj, "AddressType");

  if (cJSON_IsString(msgType) || cJSON_IsString(AddressType))
  {
    if (strcasecmp(msgType->valuestring, "stun") == 0 &&
        strcasecmp(AddressType->valuestring, "Source") == 0)
    {

      cJSON *ip = cJSON_GetObjectItemCaseSensitive(jsonObj, "ip");
      cJSON *port = cJSON_GetObjectItemCaseSensitive(jsonObj, "port");

      if (cJSON_IsString(ip) && cJSON_IsNumber(port))
      {
        strcpy(myAddress.ip, ip->valuestring);
        myAddress.port = port->valueint;
      }
      else
      {
        ESP_LOGD("app_main.udpRes",
                 "[cJSON] either ip isn't string or port isn't number");
      }
    }
    else if (strcasecmp(msgType->valuestring, "stun") == 0 &&
             strcasecmp(AddressType->valuestring, "peer") == 0)
    {
      cJSON *address = cJSON_GetObjectItemCaseSensitive(jsonObj, "address");
      if (cJSON_IsObject(address))
      {
        cJSON *ip = cJSON_GetObjectItemCaseSensitive(address, "ip");
        cJSON *port = cJSON_GetObjectItemCaseSensitive(address, "port");

        if (cJSON_IsString(ip) && cJSON_IsNumber(port))
        {
          strcpy(peerAddress.ip, ip->valuestring);
          peerAddress.port = port->valueint;
        }
        else
        {
          ESP_LOGD("app_main.udpRes",
                   "[cJSON] either ip isn't string or port isn't number");
        }
      }
      else
      {
        ESP_LOGD("app_main.udpRes",
                 "[cJSON] address isen't an object");
      }
    }
    else
    {
      ESP_LOGD("app_main.udpRes",
               "[cJSON] either msgType isn't stun or AddressType isn't peer");
    }
  }
  else
  {
    ESP_LOGD("app_main.udpRes",
             "[cJSON] msgType and AddressType aren't string");
  }
  ESP_LOGD("app_main.udpRes", "my socket address is %s:%d", myAddress.ip,
           myAddress.port);
  cJSON_Delete(jsonObj);
  ESP_LOGD("app_main", "[heap size log] %d", esp_get_free_heap_size());
};





void sendAddrtoPeer() { mqPublish = true; }