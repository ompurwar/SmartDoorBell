
#include "include.h"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

static char tag[] = "mqtt_paho";

// some utility functions
extern void GetAdddrsFromJsonRes(char *);
void getMqttAddressMsgPayload(char *mqttPayload)
{

  sprintf(mqttPayload, "{\"msgType\":\"stun\",\"address\":{\"ip\":\"%s\",\"port\":%d},\"AddressType\":\"peer\"}",
          myAddress.ip, myAddress.port);
  printf("\n{###########}%s", mqttPayload);
}

static unsigned char sendBuf[1000];
static unsigned char readBuf[1000];
Network network;
static void messageHandler_func(MessageData *md)
{

  ESP_LOGD(tag, "[MQTT] Subscription received! : %s\n",
           (char *)md->message->payload);
  GetAdddrsFromJsonRes((char *)md->message->payload);
  ESP_LOGD(tag, "[MQTT] peer address is\t %s:%d", peerAddress.ip,
           peerAddress.port);
 xEventGroupSetBits(myEventGroup,GOT_PEER_ADDRESS);
};

void task_paho(void *ignore)
{
  esp_log_level_set("*", ESP_LOG_DEBUG);
  ESP_LOGD(tag, "[MQTT] Starting ...");
  int rc;
  MQTTClient client;
  NetworkInit(&network);
  ESP_LOGD(tag, "[MQTT] NetworkConnect  ...");
  NetworkConnect(&network, "192.168.43.179", 41235);
  ESP_LOGD(tag, "[MQTT] MQTTClientInit  ...");
  MQTTClientInit(&client, &network,
                 1000,            // command_timeout_ms
                 sendBuf,         // sendbuf,
                 sizeof(sendBuf), // sendbuf_size,
                 readBuf,         // readbuf,
                 sizeof(readBuf)  // readbuf_size
  );

  MQTTString clientId = MQTTString_initializer;
  clientId.cstring = "MYCLIENT1";

  MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
  data.clientID = clientId;
  data.willFlag = 0;
  data.MQTTVersion = 3;
  data.keepAliveInterval = 0;
  data.cleansession = 1;

  ESP_LOGD(tag, "[MQTT] MQTTConnect  ...");
  rc = MQTTConnect(&client, &data);

  if (rc != SUCCESS)
  {
    ESP_LOGE(tag, "[MQTT] MQTTConnect: %d", rc);
  }

  ESP_LOGD(tag, "[MQTT] MQTTSubscribe  ...");
  rc = MQTTSubscribe(&client, "jkhaiwue8r23u923y9r9wuf89y93wyr89aur/client2",
                     QOS0, messageHandler_func);
  if (rc != SUCCESS)
  {
    ESP_LOGE(tag, "[MQTT] MQTTSubscribe: %d", rc);
  }
  while (1)
  {
    // vTaskDelay(1000 / portTICK_PERIOD_MS);
    if (mqPublish)
    {
      MQTTMessage message;
      getMqttAddressMsgPayload(mqttPayload);
      mqPublish = false;

      message.qos = 1;
      message.retained = 0;
      message.payload = mqttPayload;
      message.payloadlen = strlen(mqttPayload);
      printf("\n\n\nmqttpayload: %s", mqttPayload);
      if ((rc = MQTTPublish(&client,
                            "jkhaiwue8r23u923y9r9wuf89y93wyr89aur/client1",
                            &message)) != 0)
      {
        printf("Return code from MQTT publish is %d\n", rc);
      }
    }
    MQTTYield(&client, 1000);
  }

  vTaskDelete(NULL);
}
