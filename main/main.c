/* Hello World Example
         This example code is in the Public Domain (or CC0 licensed, at your
   option.)
         Unless required by applicable law or agreed to in writing, this
         software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
         CONDITIONS OF ANY KIND, either express or implied.
         https://github.com/VirgiliaBeatrice/esp32-devenv-vscode/blob/master/tutorial.md
*/

#include "include.h"

void task_paho(void *ignore);

#define V_REF 1100
/**
 * @brief  settingup wifi setting
 */
bool WifiConnected = false;
wifi_config_t sta_config = {
    .sta = {.ssid = "MyASUS1", .password = "ultron1236", .bssid_set = 0}};

esp_err_t event_handler(void *ctx, system_event_t *event)
{
  if (event->event_id == SYSTEM_EVENT_STA_GOT_IP)
  {
    printf("Our IP address is " IPSTR "\n",
           IP2STR(&event->event_info.got_ip.ip_info.ip));
    printf("We have now connected to a station and can do things...\n");
    WifiConnected = true;
  }
  if (event->event_id == SYSTEM_EVENT_STA_START)
  {
    ESP_ERROR_CHECK(esp_wifi_connect());
  }
  if (event->event_id == SYSTEM_EVENT_STA_DISCONNECTED)
  {
    printf("[wifi event log]wifi disconnected \n");
    ESP_ERROR_CHECK(esp_wifi_connect());
  }
  if (event->event_id == SYSTEM_EVENT_STA_CONNECTED)
  {
    printf("[wifi event log] wifi connected \n");
  }
  if (event->event_id == SYSTEM_EVENT_STA_LOST_IP)
  {
    printf("[wifi event log] wifi IP LOST\n");
  }
  return ESP_OK;
};

/**
 * @brief  function to initialise WIFI
 * @retval None
 */
void init_wifi()
{

  nvs_flash_init();
  tcpip_adapter_init();
  ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &sta_config));
  ESP_ERROR_CHECK(esp_wifi_start());
};

/**
 * @brief  function to sample voice data and send it to the peer
 * @retval None
 */
void SampleVoice_AndSend()
{
  printf("\n\t#############) Entered SampleVoice_AndSend");
  // storage for data to be sent
  uint16_t data[200];

  // setting up ADC
  adc1_config_width(ADC_WIDTH_12Bit);
  adc1_config_channel_atten(ADC1_CHANNEL_7, ADC_ATTEN_11db);
  esp_adc_cal_characteristics_t characterstics;
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_12Bit, V_REF,
                           &characterstics);
  EventBits_t bits = xEventGroupWaitBits(myEventGroup, GOT_PEER_ADDRESS, pdTRUE, pdFALSE, 60000 / portTICK_RATE_MS); // max wait 60s
  if (bits == 0)
  { // xWaitForAllBits == pdFALSE, so we wait for TX1_BIT or TX2_BIT so 0 is timeout
    printf("(#############)fail to receive eventgroup value\n");
  }
  else
  {
    printf("\n(#############)voice transpission started\n");
    setPeerAddress(&serverAddress, peerAddress.port, peerAddress.ip);
    // sampelling and sending audiodata
    while (true)
    {

      // here 1 Unit time (counter) = 125 Microseconds adc uses 38us and 58us is
      // delay and now counting 5 seconds

      for (uint8_t counter = 0; counter < 200; ++counter)
      {
        uint32_t temp;
        esp_adc_cal_get_voltage(ADC_CHANNEL_7, &characterstics, &temp);
        data[counter] = (uint16_t)temp;
        // required delay for 8khz sampeling rate
        ets_delay_us(85);
      }
      // sending udp packet
      size_t a =
          sendto(sock, &data, sizeof(data), 0, (struct sockaddr *)&serverAddress,
                 sizeof(struct sockaddr_in));
      // if error occured
      if (a == -1)
      {
        printf("\n[udp log] [error] data cant be sent\n");
        printf("\n[errno log] [error] value :\t %d\n", errno);
        printf("[heap size log] %d", esp_get_free_heap_size());
      }
      // printf("\n[data] %s", data);
      esp_task_wdt_feed();
    }
  }
};

/**
 * @brief  main function the entry point to the app
 * @note   it initialise the wifi and only then call stunn server to obtain its
 * socket address
 * @retval None
 */
void app_main()
{
  // connecting to wifi
  init_wifi();
  printf("[Wifi log] connecting ...\n");

  while (!WifiConnected)
  {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    printf("[Wifi log] connecting ...\n");
  }

  //-------(creating event group)-------//
  myEventGroup = xEventGroupCreate();

  char *data = "{\"age\":12,\"msg\":\"hello\"}";

  printf("[Wifi log] connected\n");
  // initiating the socket
  sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  setPeerAddress(&serverAddress, 41234, "192.168.43.179");

  //------------(calling the stun server to to get my socket
  //address)----------//
  char mdata[] = "{\"hello\":\"man\"}";
  printf("\nhelllo1\n");
  //--------(calculating socket address length)--------//
  socklen_t socklen = sizeof(struct sockaddr_in);
  //----------------------(send udp packet to stun Server)-------------------//
  size_t a = sendto(sock, &mdata, sizeof(mdata), 0,
                    (struct sockaddr *)&serverAddress, socklen);
  printf("\nhelllo\n");
  if (a == -1)
  {
    printf("\ncoulden't send data %d\n", errno);
  }
  char incomData[100];
  size_t b = recvfrom(sock, &incomData, sizeof(incomData), 0,
                      (struct sockaddr *)&serverAddress, &socklen);
  if (b == -1)
  {
    printf("\ncoulden't send data %d\n", errno);
  }
  GetAdddrsFromJsonRes(&incomData);

  printf("\n[udp response] %s\n", incomData);
  //-------------------------(got device socke address)---------------//

  xTaskCreatePinnedToCore(&SampleVoice_AndSend, "sampleAndSend", 4048, NULL,
                          5, NULL, 1);

  esp_task_wdt_init(1000, true);
  xTaskCreatePinnedToCore(&task_paho, "task_paho", 8048, NULL, 5, NULL, 0);
  sendAddrtoPeer();
}
